//
//  AppConstants.swift
//  AppscoreCode
//
//  Created by SH-Syed on 28/11/18.
//  Copyright © 2018 Skilledhealth. All rights reserved.
//

import Foundation

// GENERAL SETTINGS

// Display Comments
let kDebugLog = true

// BASE URL
let geoURL = "https://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/all_day.geojson"
