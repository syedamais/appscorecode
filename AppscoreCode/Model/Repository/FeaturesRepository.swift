//
//  FeaturesRepository.swift
//  AppscoreCode
//
//  Created by SH-Syed on 28/11/18.
//  Copyright © 2018 Skilledhealth. All rights reserved.
//

import Foundation

//*****************************************************************
// FEATURES REPOSITORY
//*****************************************************************


class FeaturesRepository : BaseService {
    
    // MARK: - Helper to get API Data
    func getFeatures(completion: @escaping (FeatureDataResponse) -> Void ) {
        
        DispatchQueue.global(qos: .userInitiated).async {
            
            guard let featuresDataURL = URL(string: geoURL) else {
                if kDebugLog { print("GEO features data URL not a valid URL") }
                completion(.failure(error: "GEO features Data URL not a valid URL"))
                return
            }
            
            super.loadDataFromURL(url: featuresDataURL) { data, error in
                super.parseResult(data, completion: completion)
            }
        }
    }

}


