//
//  FeatureCollection.swift
//  AppscoreCode
//
//  Created by SH-Syed on 28/11/18.
//  Copyright © 2018 Skilledhealth. All rights reserved.
//

import UIKit

//*****************************************************************
// FEATURE COLLECTION MODEL
//*****************************************************************

struct FeatureCollection : Decodable {
    let features : [Feature]?
}
