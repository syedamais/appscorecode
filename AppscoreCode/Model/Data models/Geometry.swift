//
//  Geometry.swift
//  AppscoreCode
//
//  Created by SH-Syed on 28/11/18.
//  Copyright © 2018 Skilledhealth. All rights reserved.
//

import UIKit
import MapKit

//*****************************************************************
// GEOMETRY MODEL
//*****************************************************************

struct Geometry: Codable {
    
    var type: String
    var points: [Double]
    var coordinate: CLLocationCoordinate2D
    
    private enum CodingKeys: String, CodingKey {
        case type
        case points = "coordinates"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        type = try container.decode(String.self, forKey: .type)
        points = try container.decode([Double].self, forKey: .points)
        coordinate = CLLocationCoordinate2D(latitude: points[1], longitude: points[0])
    }
    
}
