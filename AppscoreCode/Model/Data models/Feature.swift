//
//  Feature.swift
//  AppscoreCode
//
//  Created by SH-Syed on 28/11/18.
//  Copyright © 2018 Skilledhealth. All rights reserved.
//

import UIKit

//*****************************************************************
// FEATURE MODEL
//*****************************************************************

struct Feature: Codable {
    
    var type: String
    var geometry: Geometry
    var property: Property

    private enum CodingKeys: String, CodingKey {
        case type
        case geometry
        case property = "properties"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        type = try container.decode(String.self, forKey: .type)
        geometry = try container.decode(Geometry.self, forKey: .geometry)
        property = try container.decode(Property.self, forKey: .property)
    }
    
}
