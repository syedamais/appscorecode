//
//  Property.swift
//  AppscoreCode
//
//  Created by SH-Syed on 28/11/18.
//  Copyright © 2018 Skilledhealth. All rights reserved.
//

import UIKit

//*****************************************************************
// PROPERTY MODEL
//*****************************************************************

struct Property: Codable {
    
    var title: String
    var time: Int64
    
    private enum CodingKeys: String, CodingKey {
        case title
        case time
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        title = try container.decode(String.self, forKey: .title)
        time = try container.decode(Int64.self, forKey: .time)
    }
    
}
