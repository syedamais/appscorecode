//
//  Double+Date.swift
//  AppscoreCode
//
//  Created by SH-Syed on 28/11/18.
//  Copyright © 2018 Skilledhealth. All rights reserved.
//

import UIKit

extension Double {
    func getDateStringFromUnixTime() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .medium
        return dateFormatter.string(from: Date(timeIntervalSince1970: self/1000))
    }
}
