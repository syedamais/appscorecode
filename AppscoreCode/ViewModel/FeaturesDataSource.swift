//
//  FeaturesDataSource.swift
//  AppscoreCode
//
//  Created by SH-Syed on 28/11/18.
//  Copyright © 2018 Skilledhealth. All rights reserved.
//

import Foundation
import UIKit

class GenericDataSource<T> : NSObject {
    var featuresData: DynamicValue<[Feature]> = DynamicValue([])
}

protocol FeatureDataSourceDelegate: class {
    func featureCellPressed(row: Int)
}

class FeaturesDataSource : GenericDataSource<Feature>, UITableViewDataSource {
    
    weak var delegate: FeatureDataSourceDelegate?
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if featuresData.value.count > 0 {
                return featuresData.value.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FeatureCell", for: indexPath) as! FeatureViewCell
        let feature = featuresData.value[indexPath.row]
        cell.configureFeatureCell(feature: feature)
        return cell;
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
}
