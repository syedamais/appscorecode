//
//  FeatureViewModel.swift
//  AppscoreCode
//
//  Created by SH-Syed on 28/11/18.
//  Copyright © 2018 Skilledhealth. All rights reserved.
//

import Foundation

class FeatureViewModel {
    
    var repository: FeaturesRepository?
    weak var dataSource : GenericDataSource<Feature>?
    
    
    init(dataSource : GenericDataSource<Feature>?) {
        self.dataSource = dataSource
        repository = FeaturesRepository()
    }
    
    func getFeatures(completion: ((FeatureDataResponse) -> Void)? = nil) {
        guard let repo = repository else { return }
        
        repo.getFeatures() { [weak self](response) in
            guard let strongSelf = self else { return }
            
            switch response {
            case .success(let result):
                strongSelf.dataSource?.featuresData.value = result
                completion?(.success(result: result))
            case.failure(let error):
                completion?(.failure(error: error))
                break
            }
        }
    }
}
