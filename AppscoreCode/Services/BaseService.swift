//
//  BaseService.swift
//  AppscoreCode
//
//  Created by SH-Syed on 28/11/18.
//  Copyright © 2018 Skilledhealth. All rights reserved.
//

import Foundation

enum  FeatureDataResponse {
    case success(result: [Feature])
    case failure(error: String)
}

class BaseService {
    
    // MARK: - API CALL METHOD
    func loadDataFromURL(url: URL, completion: @escaping (_ data: Data?, _ error: Error?) -> Void) {
        
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig)
        
        // Use URLSession to get data from an NSURL
        
        let loadDataTask = session.dataTask(with: url) { data, response, error in
            
            guard error == nil else {
                completion(nil, error!)
                if kDebugLog { print("API ERROR: \(error!)") }
                return
            }
            
            guard let httpResponse = response as? HTTPURLResponse, 200...299 ~= httpResponse.statusCode else {
                completion(nil, nil)
                if kDebugLog { print("API: HTTP status code has unexpected value") }
                return
            }
            
            guard let data = data else {
                completion(nil, nil)
                if kDebugLog { print("API: No data received") }
                return
            }
            
            // Success, return data
            completion(data, nil)
        }
        loadDataTask.resume()
    }
    
    // MARK: - PARSE RESPONSE
    func parseResult(_ data: Data?,completion: @escaping (FeatureDataResponse) -> Void) {
        
        
        guard let data = data, let response = try? JSONDecoder().decode(FeatureCollection.self, from: data) else {
            completion(.failure(error: "Error parsing response data"))
            return
        }
        
        completion(.success(result: (response.features)!))
    }
    
}
