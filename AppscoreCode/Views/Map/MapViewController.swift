//
//  MapViewController.swift
//  AppscoreCode
//
//  Created by SH-Syed on 28/11/18.
//  Copyright © 2018 Skilledhealth. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController, MKMapViewDelegate {
    
    // MARK: - Properties
    var featureCoordinate: CLLocationCoordinate2D?
    var featureTitle: String?
    
    @IBOutlet weak var mapView: MKMapView!
    let regionRadius: CLLocationDistance = 6000
    
    // MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mapView.delegate = self
        let featurePin = FeaturePin(coordinate: featureCoordinate!, title: featureTitle!, subtitle: featureTitle!)
        self.setMapFocus(centerCoordinate: featureCoordinate!, radiusInKm: regionRadius)
        mapView.showsUserLocation = false
        mapView.addAnnotation(featurePin)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        checkLocationAuthorizationStatus()
    }
    
    // MARK: - CLLocationManager
    
    let locationManager = CLLocationManager()
    func checkLocationAuthorizationStatus() {
        if CLLocationManager.authorizationStatus() == .authorizedAlways {
            mapView.showsUserLocation = true
        } else {
            locationManager.requestAlwaysAuthorization()
        }
        
    }
    
    // MARK: - Map view helper methods
    
    func mapView(_ mapView: MKMapView, didAdd views: [MKAnnotationView]) {
        if let userLocation = mapView.view(for: mapView.userLocation) {
            userLocation.isHidden = true
        }
    }
    
    func setMapFocus(centerCoordinate: CLLocationCoordinate2D, radiusInKm radius: CLLocationDistance)
    {
        let diameter = radius * 2
        let region: MKCoordinateRegion = MKCoordinateRegion(center: centerCoordinate, latitudinalMeters: diameter, longitudinalMeters: diameter)
        self.mapView.setRegion(region, animated: false)
    }
    
    
    
}
