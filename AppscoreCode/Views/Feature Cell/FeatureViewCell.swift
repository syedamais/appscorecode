//
//  FeatureViewCell.swift
//  AppscoreCode
//
//  Created by SH-Syed on 28/11/18.
//  Copyright © 2018 Skilledhealth. All rights reserved.
//

import UIKit

class FeatureViewCell: UITableViewCell {

    @IBOutlet weak var place: UILabel!
    @IBOutlet weak var time: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        // set table selection color
        let selectedView = UIView(frame: CGRect.zero)
        selectedView.backgroundColor = UIColor(red: 78/255, green: 82/255, blue: 93/255, alpha: 0.6)
        selectedBackgroundView  = selectedView
    }

    func configureFeatureCell(feature: Feature) {
        
        // Configure the cell...
        
        place.text = feature.property.title
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd YYYY hh:mm a"
        time.text = Double(feature.property.time).getDateStringFromUnixTime()
        
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        place.text  = nil
        time.text  = nil
    }
}
