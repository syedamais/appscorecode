//
//  ViewController.swift
//  AppscoreCode
//
//  Created by SH-Syed on 28/11/18.
//  Copyright © 2018 Skilledhealth. All rights reserved.
//

import UIKit

class ViewController: UIViewController, FeatureDataSourceDelegate {
    
    @IBOutlet var featuresTable: UITableView!
    var selectedRow = 0
    
    // Setup data source for table view
    let dataSource = FeaturesDataSource()
    
    // Setup ViewModel for Feature Data
    lazy var featureVM : FeatureViewModel = {
        let featureVM = FeatureViewModel(dataSource: dataSource)
        return featureVM
    }()
    
    // init refresh control
    var refreshControl: UIRefreshControl = {
        return UIRefreshControl()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.featuresTable.dataSource = self.dataSource
        self.featuresTable.delegate = self
        
        // Bind self as oberser for change for features data
        self.dataSource.featuresData.addAndNotify(observer: self) { [weak self] in
            DispatchQueue.global().async {
                DispatchQueue.main.sync {
                    if (self?.dataSource.featuresData.value.count)! > 0 {
                        self?.featuresTable.reloadData()
                        self?.refreshControl.endRefreshing()
                        
                    }
                }
            }
        }
        
        // Setup Pull to Refresh
        setupPullToRefresh()
        
        // Load Data
        loadFeaturesData()
        
    }
    
    func featureCellPressed(row: Int) {
        loadFeaturesData()
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showMap" {
            let mapVC = segue.destination as! MapViewController
            let feature = featureVM.dataSource?.featuresData.value[selectedRow]
            mapVC.featureCoordinate = feature?.geometry.coordinate
            mapVC.featureTitle = feature?.property.title
        }
    }
    
}

// MARK: - Features Data

extension ViewController {
    
    func setupPullToRefresh() {
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh", attributes: [.foregroundColor: UIColor.white])
        refreshControl.backgroundColor = .white
        refreshControl.tintColor = .black
        refreshControl.addTarget(self, action: #selector(loadFeaturesData), for: .valueChanged)
        featuresTable.addSubview(refreshControl)
    }
    
    @objc func loadFeaturesData() {
        featureVM.getFeatures()
    }
}

// MARK: - Tableview delegate function

extension ViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: false)
        selectedRow = indexPath.row
        performSegue(withIdentifier: "showMap", sender: nil)
        
    }
}



