//
//  FeaturesDataSourceTests.swift
//  AppscoreCodeTests
//
//  Created by SH-Syed on 28/11/18.
//  Copyright © 2018 Skilledhealth. All rights reserved.
//

import XCTest
@testable import AppscoreCode

class FeaturesDataSourceTests: XCTestCase {
    
    var dataSource : FeaturesDataSource!
    
    override func setUp() {
        super.setUp()
        dataSource = FeaturesDataSource()
    }
    
    override func tearDown() {
        dataSource = nil
        super.tearDown()
    }
    
    func testEmptyValueInDataSource() {
        
        // giving empty data value
        dataSource.featuresData.value = []

        let tableView = UITableView()
        tableView.dataSource = dataSource
        
        // expected two section
        XCTAssertEqual(dataSource.numberOfSections(in: tableView), 1, "Expected two section in table view")
        
        // expected zero cell for section north
        XCTAssertEqual(dataSource.tableView(tableView, numberOfRowsInSection: 0), 0, "Expected 0 cell in table view")
        
    }
    
    func testFeaturesDataInDataSource() {
        
        // giving data value
        guard let data = FileManager.readJson(forResource: "sample") else {
            XCTAssert(false, "Can't get data from sample.json")
            return
        }
        
        guard let response = try? JSONDecoder().decode(FeatureCollection.self, from: data) else {
            XCTAssert(false, "Parsing error")
            return
        }
        
        guard let features = response.features else {
            XCTAssert(false, "Parsing features error")
            return
        }
        
        dataSource.featuresData.value = features
        
        let tableView = UITableView()
        tableView.dataSource = dataSource
        
        // expected one section
        XCTAssertEqual(dataSource.numberOfSections(in: tableView), 1, "Expected one section in table view")
        
        // expected two cells for features data
        XCTAssertEqual(dataSource.tableView(tableView, numberOfRowsInSection: 0), 1, "Expected 1 cell in table view")
    }
    
}
