//
//  AppscoreCodeTests.swift
//  AppscoreCodeTests
//
//  Created by SH-Syed on 28/11/18.
//  Copyright © 2018 Skilledhealth. All rights reserved.
//

import XCTest
@testable import AppscoreCode

class FeaturesViewModelTests: XCTestCase {
    
    var viewModel : FeatureViewModel!
    let dataSource = FeaturesDataSource()

    
    override func setUp() {
        super.setUp()
        self.viewModel = FeatureViewModel(dataSource: dataSource)
    }
    
    override func tearDown() {
        self.viewModel = nil
        super.tearDown()
    }
    
    func testFetchFeaturesData() {
        
        viewModel.getFeatures(){ result in
            switch result {
            case .success(_) :
                XCTAssert(false, "ViewModel should not able to fetch features data")
            default:
                break
            }
        }
    }

}

