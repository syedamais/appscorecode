//
//  AppscoreCodeTests.swift
//  AppscoreCodeTests
//
//  Created by SH-Syed on 28/11/18.
//  Copyright © 2018 Skilledhealth. All rights reserved.
//

import XCTest
@testable import AppscoreCode

class FeatureTests: XCTestCase {


    func testInitialization() {
        guard let data = FileManager.readJson(forResource: "sample") else {
            XCTAssert(false, "Can't get data from sample.json")
            return
        }

        guard let response = try? JSONDecoder().decode(FeatureCollection.self, from: data) else {
            XCTAssert(false, "Parsing error")
            return
        }

        guard let features = response.features else {
            XCTAssert(false, "Parsing features error")
            return
        }

        let feature = features.first

        XCTAssertEqual("M 1.2 - 10km WNW of The Geysers, CA", feature?.property.title)
        XCTAssertEqual(-122.8519974, feature?.geometry.coordinate.longitude)
        XCTAssertEqual(38.8203316, feature?.geometry.coordinate.latitude)
        
    }
    
    func testParseEmptyResponse() {
        
        let data = Data()

        guard (try? JSONDecoder().decode(FeatureCollection.self, from: data)) != nil else {
            XCTAssert(true, "Expected failure when no data")
            return
        }
        
    }


}

extension FileManager {

    static func readJson(forResource fileName: String ) -> Data? {

        let bundle = Bundle(for: AppscoreCodeTests.self)
        if let path = bundle.path(forResource: fileName, ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                return data
            } catch {
                // handle error
            }
        }
        
        return nil
    }
}
